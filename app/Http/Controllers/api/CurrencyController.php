<?php

namespace App\Http\Controllers\api;

use App\Currency;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CurrencyController extends Controller
{

    public function index(Request $request){
        $this->validate($request, [
            'valuteID' => 'required',
            'from' => 'required|date_format:Y-m-d',
            'to' => 'required|date_format:Y-m-d'
        ]);
        $currency = Currency::whereBetween('date', [strtotime($request->from), strtotime($request->to)])
        ->where(['valuteID' => $request->valuteID])->get();
        return response()->json($currency);
    }

}
