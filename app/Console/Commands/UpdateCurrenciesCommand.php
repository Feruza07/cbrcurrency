<?php
/**
 *
 * PHP version >= 7.0
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */

namespace App\Console\Commands;


//use App\Currency;
use App\Currency;
use Exception;
use Illuminate\Console\Command;


/**
 * Class deletePostsCommand
 *
 * @category Console_Command
 * @package  App\Console\Commands
 */
class UpdateCurrenciesCommand extends Command
{
    const CURRENCY_URL = 'http://www.cbr.ru/scripts/';

    protected $translate;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = "update:currencies";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Update all currencies";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Please wait...";
        $fromDate = strtotime(date('Y/m/d'));
        $toDateFormat = date('d/m/Y', strtotime(' - 30 days', $fromDate));
        $fromDateFormat = date('d/m/Y', $fromDate);
        $valuteList = $this->get(self::CURRENCY_URL.'XML_daily.asp');
        foreach ($valuteList->Valute as $valute) {
            $valute = (array) $valute;
            $dynamicList = $this->get(self::CURRENCY_URL."XML_dynamic.asp?date_req1={$toDateFormat}&date_req2={$fromDateFormat}&VAL_NM_RQ={$valute["@attributes"]['ID']}");
            foreach ($dynamicList->Record as $dynamic) {
                $dynamic = (array)$dynamic;
                $currency = Currency::firstOrNew(['сharCode' => $valute['CharCode'], 'date' => strtotime($dynamic["@attributes"]['Date'])]);
                $currency->valuteID = $dynamic["@attributes"]['Id'];
                $currency->numCode = $valute['NumCode'];
                $currency->сharCode = $valute['CharCode'];
                $currency->value = str_replace(',', '.', $dynamic['Value']);
                $currency->date = strtotime($dynamic["@attributes"]['Date']);
                if($currency->save())
                    echo $currency->id."\n";
            }
        }
        echo "Finished";

    }

    private function get($url){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', $url);
            $statusCode = $response->getStatusCode();
            if($statusCode == 200) {
                return simplexml_load_string($response->getBody()->getContents());
            }else {
                $this->info("status code{$statusCode}");
            }
        } catch (Exception $e) {
            $this->error("An error occurred");
        }
    }
}
