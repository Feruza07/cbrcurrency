import React from 'react';

const Header = () => {
    return (
        <header className="App-header">
            <nav>
                <ul>
                    <li>
                        <a href="#">Main</a>
                    </li>
                    <li>
                        <a href="#">Currencies</a>
                    </li>
                    <li>
                        <a href="#">About</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                </ul>
            </nav>
        </header>
    );
};

export default Header;
