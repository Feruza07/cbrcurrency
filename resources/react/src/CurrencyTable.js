import React, {Component} from 'react';
import axios from 'axios'
import TableItem from './TableItem'
import CurrencySelect from './CurrencySelect'
import data from './currency.json'

class CurrencyTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            currencies: []
        }
    }

    componentDidMount() {
        let today = new Date();
        let dd = today.getDate();
        let mm = today.getMonth()+1;
        if(mm < 10){
            mm = '0'+mm;
        }
        const yyyy = today.getFullYear();
        let startDate = yyyy+'-'+mm+'-'+(dd-1);
        let endDate = yyyy+'-'+mm+'-'+dd;
        axios.get(`https://currency.loc/api/currency?valuteID=R01235&from=${startDate}&to=${endDate}`)
            .then(res =>
                this.setState({ currencies: res.data}));
    }

    sendRequest = (e) => {
        const valuteID = e.currencyName;
        const startDate = e.startDate;
        const endDate = e.endDate;
        const url = `https://currency.loc/api/currency?valuteID=${valuteID}&from=${startDate}&to=${endDate}`;

        axios.get(url)
            .then(res =>
                this.setState({ currencies: res.data}));
    };

    render() {
        const tableContent = this.state.currencies.length !== 0 ?
            <table className="main-table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>№</th>
                        <th>Валюта</th>
                        <th>Курс</th>
                        <th>Дата</th>
                    </tr>
                </thead>
                <tbody>
                    {this.state.currencies.map(currency => {
                        return(<TableItem key = {currency.id} currency = {currency} />)
                    })}
                </tbody>
            </table> : null;

        return (
            <section className="main-content">
                <CurrencySelect searchData = {this.sendRequest} charCode = {this.state.currencies.charCode}/>
                {tableContent}
            </section>
        )
    }
}

export default CurrencyTable;
