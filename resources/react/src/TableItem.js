import React from 'react';
import Moment from 'moment'

const TableItem = ({currency}) => {
    return (
        <tr>
            <td>{currency.valuteID}</td>
            <td>{currency.numCode}</td>
            <td>{currency.сharCode}</td>
            <td>{currency.value}</td>
            <td>{Moment(currency.date*1000).format('YYYY-MM-DD')}</td>
        </tr>
    );
};

export default TableItem;
