import React,{Component} from 'react';

class CurrencySelect extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currencyName: "",
            startDate: "",
            endDate: ""
        }
    }

    onChange = (e) => {this.setState({[e.target.name]: e.target.value})};

    onSubmit = (e) => {
        e.preventDefault();
        this.props.searchData(this.state);
    };

    render() {
        return (
            <div className="main-form">
                <form onSubmit={this.onSubmit}>
                    <div>
                        <fieldset>
                            <label>Код валюты</label>
                            <input
                                type="text"
                                name="currencyName"
                                placeholder="R01010"
                                onChange={this.onChange}
                                value = {this.state.currencyName}
                                required
                            />
                        </fieldset>
                        <fieldset>
                            <label>From</label>
                            <input
                                type="date"
                                name="startDate"
                                onChange={this.onChange}
                                value = {this.state.startDate}
                                required
                            />
                        </fieldset>
                        <fieldset>
                            <label>To</label>
                            <input
                                type="date"
                                name="endDate"
                                onChange={this.onChange}
                                value = {this.state.endDate}
                                required
                            />
                        </fieldset>
                    </div>
                    <input
                        type="submit"
                        value="Поиск"
                        className="btn"
                    />
                </form>
            </div>
        );
    }
}

export default CurrencySelect;
