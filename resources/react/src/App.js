import React from 'react';
import Header from "./Header";
import CurrencyTable from "./CurrencyTable";

import './App.css';


function App() {
  return (
    <div className="App">
      <Header/>
      <CurrencyTable/>
    </div>
  );
}

export default App;
